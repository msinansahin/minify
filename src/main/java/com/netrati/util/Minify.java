package com.netrati.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Minify {
	static File resourceRootFile = null;
	static File minResourceRootFile = null;
	static List<String> exludes = new ArrayList<String>();

	public static void main(String[] args) throws IOException {
		resourceRootFile = new File("/tmp/sil/kitantik/resources");
		minResourceRootFile = new File("/tmp/sil/kitantik/min-resources");
		
		exludes.addAll(Arrays.asList("trumbowyg",
				"vue-table",
				"vue-table-netarti"));
		
		if (!resourceRootFile.exists()) {
			throw new RuntimeException(resourceRootFile + " not found");
		}
		
		minify(resourceRootFile);
		count(resourceRootFile);
		int countRes = count;
		count = 0;
		count(minResourceRootFile);
		int countMin = count;
		String counts = "Res Count: " + countRes + ", Min Count: " + countMin;
		if (countRes != countMin) {
			throw new RuntimeException(counts + "\nResource ve Min dizinlerindeki dosya sayısı aynı değil");
		}
		System.out.println("BASARILI Res Count: " + countRes + ", Min Count: " + countMin);
	}
	
	static int count = 0;
	
	private static void count(File root) {
		count++;
		File [] files = root.listFiles();
		if (files == null) {
			return;
		}
		for (File file : files) {
			count(file);
		}
	}

	private static void minify(File root) throws IOException {
		if (root == null) {
			return;
		}
		if (root.isFile()) {
			JsOrCssFile jsFile = new JsOrCssFile(root, resourceRootFile, minResourceRootFile, exludes);
			jsFile.minify();
			return;
		}
		File [] files = root.listFiles();
		if (files == null) {
			return;
		}
		for (File file : files) {
			minify(file);
		}
	}
}
