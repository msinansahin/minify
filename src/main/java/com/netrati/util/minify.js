/**
 * npm install node-minify
 * npm install fs-extra
 *
 */
const fs = require('fs-extra');
const pathLib = require('path');
const compressor = require('node-minify');

Array.prototype.contains = function ( needle ) {
    for (var i in this) {
        if (this[i] == needle) return true;
    }
    return false;
}

// List all files in a directory in Node.js recursively in a synchronous fashion
function Utility() {
    this.version = function () {
        return '1.0';
    },
    this.walkSync = function(dir, filelist) {
        var _this = this;
        var files = fs.readdirSync(dir);
        filelist = filelist || [];
        files.forEach(function(file) {
            if (fs.statSync(dir + file).isDirectory()) {
                filelist = _this.walkSync(dir + file + '/', filelist);
            }
            else {
                filelist.push(dir + file);
            }
        });
        return filelist;
    },
    this.getBaseName = function (filePath) {
        return pathLib.basename(filePath).split('.')[0];
    },
    this.getDirName = function (filePath) {
        return pathLib.dirname(filePath);
    },
    this.copyFile = function (from, to) {
        fs.copySync(from, to)
    }
}

function JsOrCssFile(inputFile, inputRootPath, outputRootPath) {
    this.inputFile = inputFile;
    this.outputRootPath = outputRootPath;
    this.inputRootPath = inputRootPath;
    this.minify = function () {
        var willBeMinified = false;
        var path = this.inputFile.toLowerCase();
        if (!(path.endsWith(".js") || path.endsWith(".css"))) {
            console.log('X Not js or css ' + path);
        } else if (path.endsWith("min.js") || path.endsWith("min.css")) {
            console.log('X Already compressed ' + path);
        } else if (excludes.contains(util.getBaseName(path))) {
            console.log("X Exclude " + inputFile);
        } else {
            willBeMinified = true;
        }
        var output = this.outputRootPath + this.inputFile.replace(this.inputRootPath, "");
        if (!willBeMinified) {
            util.copyFile(inputFile, output);
            return;
        }

        var compressorType = null;
        if (path.endsWith(".css")) {
            compressorType = 'clean-css';
        } else if (path.endsWith(".js")) {
            compressorType = 'uglifyjs'
        } else {
            throw Error('Check your conditions. neither css nor js > ' + path);
        }
        compressor.minify({
            compressor: compressorType,
            input: path,
            output: output,
            callback: function (err, min) {
                console.log('OK ' + path);
            }
        });

    }
}

const RESOURCE_ROOT_FILE = "/tmp/sil/kitantik/resources/";
const MIN_RESOURCE_ROOT_FILE = "/tmp/sil/kitantik/min-resources/"; // Where min resource will be written

const excludes = ["trumbowyg", "vue-table", "vue-table-netarti"];
const util = new Utility();


fs.removeSync(MIN_RESOURCE_ROOT_FILE);
console.log('min resource dir deleted (if exists): ' + MIN_RESOURCE_ROOT_FILE);
console.log('version: ' + util.version());
fs.mkdirSync(MIN_RESOURCE_ROOT_FILE);
console.log('min resource dir created: ' + MIN_RESOURCE_ROOT_FILE);

var fileList = util.walkSync(RESOURCE_ROOT_FILE);
console.log('TOTAL file: ' + fileList.length);

for (var i = 0; i < fileList.length; i++) {
    var jcf = new JsOrCssFile(fileList[i], RESOURCE_ROOT_FILE, MIN_RESOURCE_ROOT_FILE);
    jcf.minify();
}
