package com.netrati.util;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.yahoo.platform.yui.compressor.YUICompressor;

public class JsOrCssFile {
	private File inputFile;
	private File outputRootPath;
	private File inputRootPath;
	private List<String> excludes;
	
	public JsOrCssFile(File inputFile, File inputRootPath, File outputRootPath, List<String> excludes) {
		this.inputFile = inputFile;
		this.outputRootPath = outputRootPath;
		this.inputRootPath = inputRootPath;
		this.excludes = excludes;
	}
	
	public void minify() throws IOException {
		String path = inputFile.getPath().toLowerCase();
		boolean willBeMini = true;
		if (!(path.endsWith(".js") || path.endsWith(".css"))) {
			System.out.println("X " + inputFile);
			willBeMini = false;
		} else if (path.endsWith("min.js")) {
			System.out.println("M " + inputFile);
			willBeMini = false;
		} else if (excludes.contains(FilenameUtils.getBaseName(path))) {
			System.out.println("EX " + inputFile);
			willBeMini = false;
		}

		String inputRootPathStr = inputRootPath.getPath();
		String output = outputRootPath.getPath() +  inputFile.getPath().replace(inputRootPathStr, "");
		if (!willBeMini) {
			FileUtils.copyFile(inputFile, new File(output));
			return;
		}
		
		if (!new File("/" + FilenameUtils.getPath(output)).exists()) {
			new File("/" + FilenameUtils.getPath(output)).mkdirs();
		}
		String [] arStrings = new String[]{inputFile.getPath(), "-o", output};
		YUICompressor.main(arStrings);
		System.out.println("OK " + inputFile + " -> " + output);
	}
}
